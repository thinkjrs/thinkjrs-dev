import b00stImage from '../public/static/images/b00st_screen.webp'
import phundImage from '../public/static/images/phund_xyz.webp'
import slightlyImage from '../public/static/images/slightly_sharpe.webp'
import tincreDevImage from '../public/static/images/tincre-dev.webp'
import lottaImage from '../public/static/images/lotta-moberg-phd-cfa.webp'
import stevensServicesImage from '../public/static/images/stevens-services.jpg'

const projectsData = [
  {
    title: 'Tincre Developer Platform',
    description: `Tincre solves the web's last mile. APIs and integrations for developers while powering Tincre's one-click no-code solutions.`,
    imgSrc: tincreDevImage,
    href: 'https://tincre.dev',
  },
  {
    title: 'b00st.com',
    description: `A better way to market music for artists, labels, management companies and their partners.`,
    imgSrc: b00stImage,
    href: 'https://b00st.com',
  },
  {
    title: 'phund.xyz',
    description: 'Fund your raise the hard way and keep your equity for yourself.',
    imgSrc: phundImage,
    href: 'https://phund.xyz',
  },
  {
    title: 'The Slightly Sharpe Blog',
    description:
      'A digital outline of a hand-held mobile device and a bright source of alternative, new information about the web, business, finance, and software.',
    imgSrc: slightlyImage,
    href: 'https://slightlysharpe.com',
  },
  {
    title: 'lottamoberg.com',
    description: `Lotta Moberg, PhD, CFA's site for blog, opinion, research, and media posts.`,
    imgSrc: lottaImage,
    href: 'https://lottamoberg.com',
  },
  {
    title: 'Stevens Services LLC',
    description: `Whether it's lawn and landscape, snow and ice management, or construction and remodeling, Stevens Services brings an experienced team to solve customer challenges. Let them help solve yours.`,
    imgSrc: stevensServicesImage,
    href: 'https://stevens-services.com',
  },
]

export default projectsData
